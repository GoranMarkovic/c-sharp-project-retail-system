﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RelearningCS_project
{
    class Uloga
    {
        private string sifraUloge;
        private string nazivUloge;
        private static List<Uloga> dostupneUloge = new List<Uloga>();

        public Uloga(string sifra, string naziv)
        {
            this.sifraUloge = sifra;
            this.nazivUloge = naziv;
        }

        public string NazivUloge
        {
            get { return nazivUloge; }
        }
        
        public static bool DaLiUlogaPostoji(string sifraZaProveru, string nazivZaProveru)
        {
            foreach (var uloga in dostupneUloge)
            {
                if (uloga.sifraUloge.ToLower() == sifraZaProveru.ToLower() 
                    || uloga.nazivUloge.ToLower() == nazivZaProveru.ToLower()) 
                { return true; }
            }
            return false;
        }

        public static bool KreairajUlogu(string sifra, string naziv)
        {
            if (DaLiUlogaPostoji(sifra, naziv)) { return false; }
            Uloga novaUloga = new Uloga(sifra, naziv);
            dostupneUloge.Add(novaUloga);
            return true;
        }

        public static bool PrikaziPostojeceUloge()
        {
            if (dostupneUloge.Count == 0)
            {
                Console.WriteLine("Trenutno nema uloga.");
                Console.WriteLine("Popunite podatke da biste uneli novu ulogu u sistem.");
                return false;
            }
            Console.WriteLine("Dostupne uloge:");
            Console.WriteLine("---------------");
            Console.Write("\n");
            foreach (var uloga in dostupneUloge)
            {
                Console.WriteLine("Sifra uloge: {0}", uloga.sifraUloge);
                Console.WriteLine("Naziv uloge: {0}", uloga.nazivUloge);
                Console.Write("\n");
            }
            Console.WriteLine("---------------");
            return true;
        }

        public static Uloga VratiUloguPoSifri(string sifra)
        {
            foreach (var uloga in dostupneUloge)
            {
                if(uloga.sifraUloge == sifra)
                {
                    return uloga;
                }
            }
            return null;
        }
    }
}
