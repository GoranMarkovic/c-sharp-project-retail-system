﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RelearningCS_project
{
    class Cenovnik
    {
        private DateTime datumPocetka; //vazenja cene
        private DateTime datumZavrsetka;
        private float cenaProizvoda;
        private Proizvod proizvod;

        public Cenovnik() { }

        public Cenovnik(string pocetak, string zavrsetak, float cena)
        {
            datumPocetka = DateTime.Parse(pocetak);
            datumZavrsetka = DateTime.Parse(zavrsetak);
            cenaProizvoda = cena;
        }

        public Proizvod Proizvod
        {
            get{return proizvod;}
            set{proizvod=value;}
        }

        public DateTime DatumPocetka
        {
            get { return datumPocetka; }
        }

        public DateTime DatumZavrsetka
        {
            get { return datumZavrsetka; }
        }

        public float CenaProizvoda
        {
            get { return cenaProizvoda; }
        }

        public DateTime DatumPocetkaSamoDatum
        {
            get { return DatumPocetka.Date; }
        }
    }
}
