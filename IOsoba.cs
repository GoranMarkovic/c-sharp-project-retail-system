﻿using System;

namespace RelearningCS_project
{
    interface IOsoba
    {
        string Ime { get; set; }
        string Prz { get; set; }
        DateTime DatumRodjenja { get; set; }
        string MestoBoravista { get; set; }
        string KontaktTelefon { get; set; }
        string EmailAdresa { get; set; }
        void PrikazOsobe();
    }
}




