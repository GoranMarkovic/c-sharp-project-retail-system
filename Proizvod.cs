﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RelearningCS_project
{
    class Proizvod
    {
        private string kodProizvoda;
        private string nazivProizvoda;
        private static List<Proizvod> proizvodi = new List<Proizvod>();
        private List<Cenovnik> cenovnici = new List<Cenovnik>();

        public Proizvod() { }
        public Proizvod(string kod, string naziv)
        {
            kodProizvoda = kod;
            nazivProizvoda = naziv;
        }

        public static Proizvod GetProizvodByKod(string kod)
        {
            foreach (var proizvod in proizvodi)
            {
                if (proizvod.kodProizvoda == kod) { return proizvod; }
            }
            return null;
        }

        public void DodajCenovnik(Cenovnik cenovnik)
        {
            cenovnici.Add(cenovnik);
        }

        public void DodajProizvod()
        {
            proizvodi.Add(this);
        }

        public float GetTrenutnaCena(Proizvod proizvod)
        {
            foreach (var cenovnik in proizvod.cenovnici)
            {
                if (cenovnik.DatumPocetkaSamoDatum.Equals(DateTime.UtcNow.Date))
                {
                    return cenovnik.CenaProizvoda;
                }
            }
            return -1;
        }

        public Cenovnik GetTrenutniCenovnik()
        {
            foreach (var cenovnik in cenovnici)
            {
                if (cenovnik.DatumPocetkaSamoDatum.Equals(DateTime.UtcNow.Date)) { return cenovnik; }
            }
            return null;
        }

        public void PrikazJednogProizvoda()
        {
            Console.WriteLine("Naziv proizvoda: {0}", this.nazivProizvoda);
            Console.WriteLine("Trenutna cena: {0}/kg", GetTrenutnaCena(this));
        }

        public static void PrikazProizvoda()
        {
            foreach (var proizvod in proizvodi)
            {
                Console.WriteLine("Kod proizvoda: {0}", proizvod.kodProizvoda);
                Console.WriteLine("Naziv proizvoda: {0}", proizvod.nazivProizvoda);
                Console.Write("\n");

                Console.WriteLine("Cenovnici" + "\n");
                foreach (var cenovnik in proizvod.cenovnici)
                {
                    Console.WriteLine("\t" + "Datum pocetka vazenja (mm/dd/year hh:mm:ss): {0}", cenovnik.DatumPocetka);
                    Console.WriteLine("\t" + "Datum zavrsetka vazenja (mm/dd/year hh:mm:ss): {0}", cenovnik.DatumZavrsetka);
                    Console.WriteLine("\t" + "Cena proizvoda: {0}/kg", cenovnik.CenaProizvoda);
                    Console.Write("\n");
                }
            }
        }

        public static void PrikazProizvodaSaTrenutnimCenovnikom()
        {
            foreach (var proizvod in proizvodi)
            {
                Console.WriteLine("Kod proizvoda: {0}", proizvod.kodProizvoda);
                Console.WriteLine("Naziv proizvoda: {0}", proizvod.nazivProizvoda);
                Console.Write("\n");

                Console.WriteLine("Cenovnik" + "\n");
                foreach (var cenovnik in proizvod.cenovnici)
                {
                    if (cenovnik.DatumPocetkaSamoDatum.Equals(DateTime.UtcNow.Date))
                    {
                        Console.WriteLine("\t" + "Datum pocetka vazenja (mm/dd/year hh:mm:ss): {0}", cenovnik.DatumPocetka);
                        Console.WriteLine("\t" + "Datum zavrsetka vazenja (mm/dd/year hh:mm:ss): {0}", cenovnik.DatumZavrsetka);
                        Console.WriteLine("\t" + "Cena proizvoda: {0}/kg", cenovnik.CenaProizvoda);
                        Console.Write("\n" + "---------------------------" + "\n");
                    }
                }
            }
        }

        public string KodProizvoda
        {
            get { return kodProizvoda; }
        }
        
        public string NazivProizvoda
        {
            get { return nazivProizvoda; }
        }
    }
}
