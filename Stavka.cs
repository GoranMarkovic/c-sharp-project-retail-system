﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RelearningCS_project
{
    class Stavka
    {
        private int redniBroj;
        private static int brojacStavki = 0;
        private float kolicina;
        private float iznos;
        private Cenovnik cenovnik;
        private static List<Stavka> stavkeNaRacunu = new List<Stavka>();
        
        public Stavka()
        {
            redniBroj = ++brojacStavki;
        }

        public static void DodajStavkuNaRacun(Stavka stavka) { stavkeNaRacunu.Add(stavka); }

        public float Kolicina
        {
            get { return kolicina; }
            set { kolicina = value; }
        }

        public Cenovnik Cenovnik
        {
            get { return cenovnik; }
            set { cenovnik = value; }
        }

        public float Iznos
        {
            get { return iznos; }
        }

        public void SetIznos()
        {
            iznos = cenovnik.CenaProizvoda * kolicina;
        }

        public int RedniBroj 
        {
            get { return redniBroj; }
        }

        public static List<Stavka> GetStavkeNaRacunu() { return stavkeNaRacunu; }
    }
}
