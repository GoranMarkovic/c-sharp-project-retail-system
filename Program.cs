﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace RelearningCS_project
{
    class Program
    {
        static List<IOsoba> UnosZaposlenih() 
        {
            Administrator administrator1 = new Administrator
            {
                SfrAdm = "A1",
                Ime = "Goran",
                Prz = "Markovic",
                DatumRodjenja = DateTime.Parse("05/01/1998"),
                MestoBoravista = "Novi Sad",
                KontaktTelefon = "062 89 48 390",
                EmailAdresa = "goranm98@gmail.com"
            };

            Prodavac prodavac1 = new Prodavac
            {
                SfrProd = "P1",
                Ime = "Milan",
                Prz = "Mirkovic",
                DatumRodjenja = DateTime.Parse("03-22-2001"),
                MestoBoravista = "Vrbas",
                KontaktTelefon = "063 85 733 85",
                EmailAdresa = "milanMi@gmail.com"
            };
            
            List<IOsoba> listaZaposlenih = new List<IOsoba>();
            listaZaposlenih.Add(prodavac1);
            listaZaposlenih.Add(administrator1);

            Console.WriteLine("Unesite novog prodavca - popunite podatke" + "\n");
            string odgovor;
            do
            {
                listaZaposlenih.Add(UnosProdavca());
                Console.Write("\n");
                Console.Write("Unesite novog? (Y or other): ");
                odgovor = Console.ReadLine();
            } while (odgovor == "Y");

            return listaZaposlenih;
        }

        static void PrikazZaposlenih(List<IOsoba> listaZaposlenih)
        {
            Console.WriteLine("Podaci o zaposlenima");
            noviRed();

            foreach (var zaposleni in listaZaposlenih)
            {
                zaposleni.PrikazOsobe();
                noviRed();
            }
        }

        static Prodavac UnosProdavca()
        {
            Console.Write("Unesite ime: ");
            string ime = Console.ReadLine();
            Console.Write("Unesite prezime: ");
            string prz = Console.ReadLine();
            Console.Write("Unesite datum rodjenja (mm/dd/year): ");
            string datumRodjenja = Console.ReadLine();
            Console.Write("Unesite mesto boravista: ");
            string mestoBoravista = Console.ReadLine();
            Console.Write("Unesite kontakt telefon: ");
            string kontaktTelefon = Console.ReadLine();
            Console.Write("Unesite email adresu: ");
            string emailAdresa = Console.ReadLine();
            Console.Write("Unesite sifru prodavca: ");
            string sifraProdavca = Console.ReadLine();
            Console.Write("\n");

            Prodavac prodavac = new Prodavac
            {
                Ime = ime,
                Prz = prz,
                DatumRodjenja = DateTime.Parse(datumRodjenja),
                MestoBoravista = mestoBoravista,
                KontaktTelefon = kontaktTelefon,
                EmailAdresa = emailAdresa,
                SfrProd = sifraProdavca
            };
            return prodavac;
        }

        static void UnosProizvoda()
        {
            static void UnosNovogCenovnika(Proizvod proizvod)
            {
                string odgovor;
                do
                {
                    //ovde obraditi izuzetak u slucaju pogresnog unosa datuma
                    Console.Write("Datum pocetka vazenja (mm/dd/year hh:mm:ss): ");
                    String datumPocetka = Console.ReadLine();
                    Console.Write("Datum zavrsetka vazenja (mm/dd/year hh:mm:ss): ");
                    String datumZavrsetka = Console.ReadLine();
                    Console.Write("Cena proizvoda: ");
                    String cenaProizvoda = Console.ReadLine();

                    Cenovnik cenovnik = new Cenovnik(datumPocetka, datumZavrsetka, float.Parse(cenaProizvoda));
                    proizvod.DodajCenovnik(cenovnik);
                    Console.Write("\n");
                    Console.Write("Unesite novi cenovnik? (Y or N): ");
                    odgovor = Console.ReadLine();
                    Console.Write("\n");
                } while (odgovor == "Y");
            }
            string odgovor;
            do
            {
                Console.Write("Kod proizvoda: ");
                string kodProizvoda = Console.ReadLine();
                Console.Write("Naziv proizvoda: ");
                string nazivProizvoda = Console.ReadLine();
                Proizvod proizvod = new Proizvod(kodProizvoda, nazivProizvoda);
                Console.Write("\n");
                Console.WriteLine("Cenovni podaci");
                UnosNovogCenovnika(proizvod);
                proizvod.DodajProizvod();
                Console.Write("Unesite novi proizvod? (Y or N): ");
                odgovor = Console.ReadLine();
                Console.Write("\n");
            } while (odgovor == "Y");
        }

        static void UbacivanjeProizvoda_u_Korpu() 
        {
            do
            {
                Console.Write("Ubacite u korpu proizvod sa sifrom (ako ne zelite , pritisnite NE): ");
                string kodProizvoda = Console.ReadLine();
                if(kodProizvoda == "NE") { break; }
                Console.Write("\n");
                Proizvod proizvod = new Proizvod();
                proizvod = Proizvod.GetProizvodByKod(kodProizvoda);
                proizvod.PrikazJednogProizvoda();

                Stavka novaStakva = new Stavka();
                Console.Write("Unesite zeljenu kolicinu (u KG): ");
                string odgovor = Console.ReadLine();
                float kolicina = float.Parse(odgovor);
                novaStakva.Kolicina = kolicina;

                Cenovnik cenovnik = new Cenovnik();
                cenovnik = proizvod.GetTrenutniCenovnik();
                cenovnik.Proizvod = proizvod;
                novaStakva.Cenovnik = cenovnik;
                novaStakva.SetIznos();
                Console.WriteLine("Iznos: {0} RSD", novaStakva.Iznos);
                Console.Write("\n");
                Stavka.DodajStavkuNaRacun(novaStakva);

            } while (true);
            Console.Write("\n");
        }

        static void Main(string[] args)
        {
            List<IOsoba> listaZaposlenih = UnosZaposlenih();
            noviRed();
            Console.WriteLine("Uspesno ste uneli prodavce! Uskoro ce uslediti prikaz unetih podataka.");
            Thread.Sleep(3500);
            Console.Clear();

            PrikazZaposlenih(listaZaposlenih);

            IOsoba prodavac = listaZaposlenih[2];

            Thread.Sleep(4000);
            Console.Clear();
            Console.WriteLine("Sada je potrebno uneti podatke o proizvodima.");
            UnosProizvoda();

            Console.Clear();
            Console.WriteLine("Podaci o proizvodima su uspesno uneti!");
            Thread.Sleep(3000);
            Console.Clear();

            Console.WriteLine("Dobro dosli u prodavnicu! Spisak dostupnih proizvoda: ");
            Console.Write("\n" + "---------------------------" + "\n");
            Proizvod.PrikazProizvodaSaTrenutnimCenovnikom();

            UbacivanjeProizvoda_u_Korpu();
            Console.Clear();
            Console.WriteLine("Molimo Vas sacekajte u redu, bicete usluzeni uskoro.");
            Thread.Sleep(5000);
            Console.Clear();

            Console.WriteLine("Postovani, hvala na kupovini, prijatan dan Vam zelimo!");
            Console.Write("\n");
            Racun racun = new Racun();
            racun.StavkeNaRacunu = Stavka.GetStavkeNaRacunu();
            racun.SetUkupanIznos();
            racun.ProdavacKojiObradjuje = (Prodavac)prodavac;
            racun.PrikazRacuna();
        }

        static void noviRed() { Console.Write("\n"); }
    }
}
            
            
