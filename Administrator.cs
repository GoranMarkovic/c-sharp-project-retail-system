﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RelearningCS_project
{
    class Administrator : IOsoba
    {
        public string SfrAdm { get; set; }

        public string Ime { get; set; }

        public string Prz { get; set; }

        public DateTime DatumRodjenja { get; set; }

        public string MestoBoravista { get; set; }

        public string KontaktTelefon { get; set; }

        public string EmailAdresa { get; set; }

        public void PrikazOsobe()
        {
            Console.WriteLine("Sifra: {0}", SfrAdm); ;
            Console.WriteLine("Ime: {0}", Ime);
            Console.WriteLine("Prezime: {0}", Prz);
            Console.WriteLine("Datum rodjenja: {0}", DatumRodjenja.Date);
            Console.WriteLine("Mesto boravista: {0}", MestoBoravista);
            Console.WriteLine("Kontakt telefon: {0}", KontaktTelefon);
            Console.WriteLine("Email adresa: {0}", EmailAdresa);
        }
    }
}

