﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RelearningCS_project
{
    class Racun
    {
        private int idRacuna = 0;
        private float ukupanIznos = 0;
        private DateTime datumIzdavanja;
        private List<Stavka> stavkeNaRacunu = new List<Stavka>();
        private Prodavac prodavacKojiObradjuje;

        public Racun() 
        { 
            ++idRacuna; 
            datumIzdavanja = DateTime.Now; 
        }

        public List<Stavka> StavkeNaRacunu 
        {
            set { stavkeNaRacunu = value; }
        }

        public void SetUkupanIznos()
        {
            foreach (var stavka in stavkeNaRacunu)
            {
                ukupanIznos += stavka.Iznos;           
            }            
        }

        public Prodavac ProdavacKojiObradjuje
        {
            set { prodavacKojiObradjuje = value; }
        }
       
        public void PrikazRacuna()
        {
            Console.WriteLine("Id racuna: {0}", idRacuna);
            Console.Write("\n");

            Console.WriteLine("Podaci o prodavcu" + "\n");
            prodavacKojiObradjuje.PrikazOsobe();
            Console.Write("\n");

            Console.WriteLine("Podaci o stavkama");
            Console.Write("\n");

            foreach (var stavka in stavkeNaRacunu)
            {
                Console.WriteLine("---------------------------");
                Console.WriteLine("Redni broj stavke: {0}", stavka.RedniBroj);
                Console.WriteLine("Kod proizvoda: {0}", stavka.Cenovnik.Proizvod.KodProizvoda);
                Console.WriteLine("Naziv proizvoda: {0}", stavka.Cenovnik.Proizvod.NazivProizvoda);
                Console.WriteLine("Cena proizvoda: {0}", stavka.Cenovnik.CenaProizvoda);
                Console.WriteLine("Kolicina: {0}kg", stavka.Kolicina);
                Console.WriteLine("Ukupan iznos: {0} RSD", stavka.Iznos);
                Console.WriteLine("---------------------------");
                Console.Write("\n");
            }

            Console.WriteLine("Datum izdavanja: {0}", datumIzdavanja);
            Console.WriteLine("Ukupan iznos racuna: {0} RSD", ukupanIznos);
        }

    }

}
